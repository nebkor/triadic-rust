use clap::Parser;
use std::collections::HashMap;
use triadic_memory::{encoders::util::yhash, sdr};

#[derive(Debug, Parser)]
#[clap(
    author,
    version,
    about = "A simple demonstrator for the deterministic hash used in the scalar encoder. Prints a table of generated SDR addresses and their collision counts."
)]
struct Cli {
    /// Length of sequence to encode
    #[clap(long, short, default_value_t = 1000)]
    length: u64,

    /// Encode a linear sequence of values <STEP> apart
    #[clap(long, short, default_value_t = 1)]
    step: u64,

    /// Value to begin the sequence
    #[clap(long, default_value_t = 0)]
    start: u64,

    /// Values out of the hasher will be modulo this value, as they would be for the size of an SDR
    #[clap(long = "mod", short, default_value_t = sdr::SIZE as u64)]
    modulo: u64,
}

fn main() {
    let cli = Cli::parse();

    let len = cli.length;
    let step = cli.step;
    let start = cli.start;
    let modulo = cli.modulo;

    let hsize = modulo as usize;
    let mut counters: HashMap<u64, i64> = HashMap::with_capacity(hsize);

    for i in 0..len {
        let v = step * i + start;
        let hashed = yhash(v) % modulo;
        *counters.entry(hashed).or_insert(-1) += 1;
    }

    let mut results: Vec<(u64, i64)> = counters.into_iter().collect();
    results.sort_unstable_by(|a, b| a.1.cmp(&b.1));
    for (hashed, collision_count) in results.iter() {
        println!("{hashed}: {collision_count}");
    }
}
