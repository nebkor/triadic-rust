# Triadic Memory

A Rust implementation of the ideas from [Peter Overmann's Triadic
Memory](https://github.com/PeterOvermann/TriadicMemory/), a collection of algorithms and
datastructures for cognitive computing with sparse distributed associative memories.
