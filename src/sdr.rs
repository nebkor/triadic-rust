use rand::{
    distributions::Uniform,
    prelude::{Distribution, Rng},
};
use std::{
    cmp::Ordering,
    collections::{BTreeSet, BinaryHeap},
    iter,
};

use crate::tri_mem::ConWeight;

pub const SIZE: usize = 1000;
pub const POP: usize = 20;

/// Used for indexing connections inside SDRs.
pub type AddrType = u16;

pub type Connections = BTreeSet<AddrType>;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Sdr {
    connections: Connections,
    pub size: usize,
}

impl Default for Sdr {
    fn default() -> Self {
        Sdr::new(SIZE, POP)
    }
}

impl Sdr {
    pub fn new(size: usize, _pop: usize) -> Self {
        Sdr {
            connections: BTreeSet::new(),
            size,
        }
    }

    pub fn random<R>(size: usize, pop: usize, rng: &mut R) -> Self
    where
        R: Rng,
    {
        let mut connections: Connections = BTreeSet::new();

        let range = Uniform::from(0..size as AddrType);
        let mut sz = 0;
        while sz < pop {
            connections.insert(range.sample(rng));
            sz = connections.len();
        }

        Sdr { connections, size }
    }

    pub fn with_connections(size: usize, connections: Connections) -> Self {
        Sdr { connections, size }
    }

    /// Constructor from triadic memory connection weights
    pub(crate) fn from_weights(weights: &[ConWeight], pop: usize) -> Self {
        // first, find the "pop"-largest connection weight to use as a threshold value
        // using a heap means we only have to sort `pop` elements instead of the whole thing
        let mut sorted_weights: BinaryHeap<_> = weights.iter().copied().collect();
        let mut ranked_max = 1;
        for _ in 0..pop {
            if let Some(w) = sorted_weights.pop() {
                ranked_max = w;
            } else {
                break;
            }
        }
        ranked_max = ranked_max.max(1);

        // now, walk through the weights and if the value is higher than `ranked_max`, add the index
        // of it to the `connections`.
        let connections = weights
            .iter()
            .enumerate()
            .filter_map(|(i, &w)| {
                if w >= ranked_max {
                    Some(i as AddrType)
                } else {
                    None
                }
            })
            .collect();

        Self::with_connections(weights.len(), connections)
    }

    /// "sparse merge"
    pub fn smerge(&self, other: &Self) -> Self {
        assert_eq!(self.size, other.size);

        let combo: Connections = self
            .connections
            .union(&other.connections)
            .copied()
            .collect();

        let connections = combo
            .iter()
            .step_by(2)
            .chain(combo.iter().skip(1).step_by(2).rev())
            .take(self.pop())
            .copied()
            .collect();

        Self {
            connections,
            size: self.size,
        }
    }

    /// "split union"
    pub fn splunion(&self, other: &Self) -> Self {
        assert_eq!(self.size, other.size);

        let size = self.size;

        let s_end = (self.pop() / 2) as AddrType;
        let o_start = (other.pop() / 2) as AddrType;

        let pop = (self.pop() + other.pop()) / 2;

        let connections = iter::empty()
            .chain(self.connections.range(..s_end))
            .chain(other.connections.range(o_start..))
            .chain(self.connections.range(s_end..))
            .take(pop)
            .copied()
            .collect();

        Self { connections, size }
    }

    pub fn union(&self, other: &Self) -> Self {
        Self {
            connections: self
                .connections
                .union(&other.connections)
                .copied()
                .collect(),
            size: self.size,
        }
    }

    fn distance(&self, other: &Self, zero: usize, incr: isize) -> usize {
        let mut score = zero;

        let mut self_iter = self.connections.iter();
        let mut other_iter = other.connections.iter();

        let mut self_one = self_iter.next();
        let mut other_one = other_iter.next();
        while self_one.is_some() && other_one.is_some() {
            let raw_self_one = self_one.unwrap();
            let raw_other_one = other_one.unwrap();
            match raw_self_one.cmp(raw_other_one) {
                Ordering::Equal => {
                    score = ((score as isize) + incr) as usize;
                    self_one = self_iter.next();
                    other_one = other_iter.next();
                }
                Ordering::Less => {
                    self_one = self_iter.next();
                }
                _ => {
                    other_one = other_iter.next();
                }
            }
        }
        score
    }

    pub fn overlap(&self, other: &Self) -> usize {
        self.connections.intersection(&other.connections).count()
    }

    pub fn hamming(&self, other: &Self) -> usize {
        let zero = self.pop() + other.pop();
        self.distance(other, zero, -2)
    }

    pub fn pop(&self) -> usize {
        self.connections.len()
    }

    pub fn connections(&self) -> &Connections {
        &self.connections
    }
}

#[cfg(test)]
mod test {
    //use super::*;

    #[test]
    fn smerge() {}

    #[test]
    fn union() {}
}
