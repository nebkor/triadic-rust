use crate::{sdr, sdr::Sdr, tri_mem::TriadicMemory};
use rand::prelude::SeedableRng;
use rand_chacha::ChaCha8Rng;

pub struct SequenceMemory {
    size: usize,
    pop: usize,
    contexts: TriadicMemory,
    predictions: TriadicMemory,
    // persistent state variables
    previous_input: Sdr,
    propagated_context: Sdr,
    previous_backstory: Sdr, // union of previous_input + previous_context
    predicted_input: Sdr,
    counter: u64,
}

impl Default for SequenceMemory {
    fn default() -> Self {
        Self::new(sdr::SIZE, sdr::POP)
    }
}

impl SequenceMemory {
    pub fn new(size: usize, pop: usize) -> Self {
        let contexts = TriadicMemory::new(size, pop);
        let predictions = TriadicMemory::new(size, pop);
        let nothing = Sdr::new(size, pop);
        let previous_input = nothing.clone();
        let propagated_context = nothing.clone();
        let previous_backstory = nothing.clone();
        let predicted_input = nothing;

        Self {
            size,
            pop,
            contexts,
            predictions,
            previous_input,
            propagated_context,
            previous_backstory,
            predicted_input,
            counter: 0,
        }
    }

    pub fn add_to_sequence(&mut self, input: &Sdr) {
        // did we see this input coming?
        if self.predicted_input.overlap(input) < self.pop {
            // nope! update our notion of what should have followed
            self.predictions
                .store(&self.previous_backstory, &self.previous_input, input);
        }

        let backstory = self.previous_input.smerge(&self.propagated_context);

        let predicted_context = self
            .contexts
            .fetch(Some(input), Some(&backstory), None)
            .unwrap();
        let story_check = self
            .contexts
            .fetch(Some(input), None, Some(&predicted_context))
            .unwrap();

        // if the previous input had matched the previous backstory's, we'd have `pop / 2` overlap
        // here at least due to shared context
        if backstory.overlap(&story_check) < self.pop / 2 {
            self.propagated_context = Sdr::random(
                self.size,
                self.pop,
                &mut ChaCha8Rng::seed_from_u64(self.counter),
            );
            self.contexts
                .store(input, &backstory, &self.propagated_context);
        } else {
            self.propagated_context = predicted_context;
        }

        self.predicted_input = self
            .predictions
            .fetch(Some(&backstory), Some(input), None)
            .unwrap();
        self.previous_input = input.clone();
        self.previous_backstory = backstory;
        self.counter += 1;
    }

    pub fn predict_next(&mut self, input: &Sdr) -> Sdr {
        let backstory = self.previous_input.smerge(&self.propagated_context);

        let predicted_context = self
            .contexts
            .fetch(Some(input), Some(&backstory), None)
            .unwrap();
        let story_check = self
            .contexts
            .fetch(Some(input), None, Some(&predicted_context))
            .unwrap();

        if backstory.overlap(&story_check) < self.pop / 2 {
            self.propagated_context = Sdr::random(
                self.size,
                self.pop,
                &mut ChaCha8Rng::seed_from_u64(self.counter),
            );
        } else {
            self.propagated_context = predicted_context;
        }

        self.predicted_input = self
            .predictions
            .fetch(Some(&backstory), Some(input), None)
            .unwrap();
        self.previous_input = input.clone();
        self.counter += 1;

        self.predicted_input.clone()
    }

    /// Must be called before adding a new sequence to the memory. Should be called when attempting
    /// to recall a sequence.
    pub fn new_seq(&mut self) {
        let nothing = Sdr::new(self.size, self.pop);
        self.previous_input = nothing.clone();
        self.propagated_context = nothing.clone();
        self.predicted_input = nothing.clone();
        self.previous_backstory = nothing;
        self.counter = 0;
    }
}
